# Turtlebot

# To build for the first time

**Additional Packages:** You have to clone three additional repositories 
into the src folder of turtlebot. The links are as follows:

- https://github.com/ROBOTIS-GIT/turtlebot3.git
- https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
- https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git

To compile:
```catkin_make```

TurtleBot3 has three models, Burger, Waffle, and Waffle Pi, 
so you have to set which model you want to use before you launch TurtleBot3. 
In your home directory type:

```sudo nano .bashrc```

Then add the following line to the file:

```export TURTLEBOT3_MODEL=burger```


## Instructions for keyboard-controlled Gazebo Simulation
Open a new terminal

```roscore```

Open a new terminal and navigate to turtlebot

```source devel/setup.bash```

Launch the Gazebo environment:

```roslaunch turtlebot3_gazebo turtlebot3_house.launch```

To move the TurtleBot with your keyboard, use this command in another terminal tab:

```roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch```


## Instructions for running ros_node python scripts
Launch the Gazebo environment again using the same instructions as before.
Run the following commands for `subscriber.py` and `robot_control.py` respectively:

- ```rosrun ros_node subscriber.py```
- ```rosrun ros_node robot_control.py```

`subscriber.py` creates a ROS node and subscribes to a particular ROS topic. This
script is easily modifiable and gives a good idea of what type of information is
being passed between nodes.

`robot_control.py` instantiates a class that allows for the manipulation of the 
turtlebot's linear and angular velocity. This script concisely shows how the
dynamics of the turtlebot can be easily modified.




