#!/usr/bin/env python3
import sys
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

class Control_Turtlebot():
    def __init__(self):
        self.cmd_vel = rospy.Publisher('cmd_vel', Twist, queue_size=10)
        rospy.on_shutdown(self.shutdown)
    def change_angular_velocity(self,vel_x,vel_y):
        move_cmd = Twist()
        move_cmd.angular.x = vel_x
        move_cmd.angular.y = vel_y
        self.cmd_vel.publish(move_cmd)

    def change_linear_velocity(self,vel_x,vel_y):
        move_cmd = Twist()
        move_cmd.linear.x = vel_x
        move_cmd.linear.y = vel_y
        self.cmd_vel.publish(move_cmd)

    def shutdown(self):
        self.change_linear_velocity(0.0,0.0)
        self.change_angular_velocity(0.0,0.0)
        rospy.sleep(1)




if __name__ == '__main__':
    rospy.init_node('Control_Turtlebot', anonymous=False)
    robot = Control_Turtlebot()
    rate = rospy.Rate(10)
    lin_x = 0.2
    while not rospy.is_shutdown():
        robot.change_angular_velocity(0.0,0.0)
        robot.change_linear_velocity(lin_x,0.0)
        rate.sleep()
